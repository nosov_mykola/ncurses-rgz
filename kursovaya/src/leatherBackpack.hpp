#include"backpack.hpp"
enum leatherType{
    Smooth,
    Solid,
    Glossy,
    Matt,
    Artifical
};
class leatherBackpack: public Backpack
{
private:
    bool have_lining;
    leatherType backpack_leather;

public:
    leatherBackpack(string color_,
                   AppointmentType appointment_,
                   bool laptop_comparament_,
                   unsigned volume_,
                   bool have_lining,
                   leatherType backpack_leather,
                   const Brand &brand_);
    ~leatherBackpack();
    virtual std::string display() override {
        return "Leather" + std::to_string(GetVolume());
    };
    virtual bool is_leather() override {
        return true;
    };
    virtual bool is_cloth() override {
        return false;
    };
};

leatherBackpack::leatherBackpack(string color_,
                   AppointmentType appointment_,
                   bool laptop_comparament_,
                   unsigned volume_,
                   bool have_lining,
                   leatherType backpack_leather,
                   const Brand &brand_)
    {
        this->color = color_;
        this->appointment = appointment_;
        this->laptop_comparament = laptop_comparament_;
        this->volume = volume_;
        this->have_lining = have_lining;
        this->backpack_leather = backpack_leather; 
        brand = new struct Brand(brand_.city, brand_.country, brand_.name_brand);
    }

leatherBackpack::~leatherBackpack()

{
    delete brand;
}