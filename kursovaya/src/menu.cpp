#include"menu.hpp"
menu::menu(WINDOW *win, menu_item *items, int num_items)
{
    this->win = win;
    this->items = items;
    this->num_items = num_items;
}

menu::~menu()
{
    if(this->items != nullptr)
        delete [] items;
}

void menu::draw(){
    for(int i = 0; i < num_items; i++){

        if(i == highlight)
            wattron(win, A_REVERSE);
        mvwprintw(win, i+1, 1, items[i].text.c_str());
        wattroff(win, A_REVERSE);
    }
}

void menu::handle_trigger(WINDOW *listwin) {
    char trigger = items[highlight].trigger;
    // controller
    // backpack *result = [];
    // for (int i = 0; i < result.length(); i++) {

    // }
}

menu_item::menu_item(string title, char trigger)
{
    this->text = title;
    this->trigger = trigger;
}

menu_item::~menu_item()
{
}