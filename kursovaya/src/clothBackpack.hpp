#include "backpack.hpp"
enum ClothType{
    Synthetic,
    Tarpaulin,
    Wool,
    Cotton
};
class clothBackpack: public Backpack
{
private:
    bool waterproof;
    ClothType cloth;
public:
    clothBackpack(string color_,
    AppointmentType appointment_,
    bool laptop_comparament_,
    unsigned volume_,
    bool waterproof,
    ClothType cloth,
    const struct Brand &brand_);
    ~clothBackpack();
    virtual std::string display() override {
        return "CLoth" + std::to_string(GetVolume());
    };
    virtual bool is_leather() override {
        return false;
    };
    virtual bool is_cloth() override {
        return true;
    };
};
clothBackpack::clothBackpack(string color_,
    AppointmentType appointment_,
    bool laptop_comparament_,
    unsigned volume_,
    bool waterproof,
    ClothType cloth,
    const struct Brand &brand_)
    {
        this->color = color_;
        this->appointment = appointment_;
        this->laptop_comparament = laptop_comparament_;
        this->volume = volume_;
        this->waterproof = waterproof;
        this->cloth = cloth;
        brand = new struct Brand(brand_.city, brand_.country, brand_.name_brand);
    }
clothBackpack::~clothBackpack()
{
    delete brand;
}