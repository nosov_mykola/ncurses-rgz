#include <ncurses.h>
#include "listController.hpp"
#include "menu.hpp"


int main()
{
    // std::list<Backpack*> list_1 = {};

    // leatherBackpack backpack_1("VL", Urban, 1, 10, 1, Smooth, Brand("", "",""));
    // leatherBackpack backpack_2("VL", Urban, 1, 15, 1, Smooth, Brand("", "",""));
    // leatherBackpack backpack_3("VL", Urban, 1, 20, 1, Smooth, Brand("", "",""));
    // clothBackpack backpack_4("VL", Urban, 1, 10, 1, Cotton, Brand("", "",""));
    
    // list_1.push_back(&backpack_1);
    // list_1.push_back(&backpack_2);
    // list_1.push_back(&backpack_3);
    // list_1.push_back(&backpack_4);

    // for (auto i : list_1)
    // {
    //     i->display();
    // }

    std::string filename = "backpack.csv";

    listController current(filename);

    current.readFromFile();

    current.showList();

    cout << endl;

    current.sortByVolume();

    current.showList();
    cout << " -=-=-=-=-=-" << endl;
    BackpackList result = current.searchLeather();
    for(auto i : result) {
        cout << i->display() << endl;
    }
    // initscr();
    // cbreak();
    // noecho();

    // curs_set(0);

    // int height, width;
    // getmaxyx(stdscr, height, width);

    // WINDOW *win = newwin(height, 10, 0, 0);
    // WINDOW *listwin = newwin(height, width - 12, 0, 11);
    // box(win, 0, 0);
    // box(listwin, 0, 0);
    // refresh();
    // wrefresh(win);
    // wrefresh(listwin);

    // search_list slist = search_list(listwin, backpacks, 10);
    // slist.draw();

    // keypad(win, true);

    // menu_item items[3] = {
    //     menu_item("Leather", 'l'),
    //     menu_item("Blue", 'b'),
    //     menu_item("Ukraine", 'u')
    // };
    // menu main_menu = menu(win, items, 3);
    // main_menu.draw();

    

    // int choice;
    // int hightlight = 0;

    // while(1)
    // {
    //     choice = wgetch(win);

    //     switch (choice)
    //     {
    //     case KEY_UP:
    //         main_menu.highlight--;
    //         if(main_menu.highlight == -1)
    //             main_menu.highlight = 0;

    //         main_menu.draw();
    //         break;
    //     case KEY_DOWN:
    //         main_menu.highlight++;
    //         if(main_menu.highlight == 3)
    //             main_menu.highlight = 2;
    //         main_menu.draw();
    //         break;
    //     case KEY_ENTER:
    //         main_menu.handle_trigger(listwin);
    //         break;
    //     default:
    //         break;
    //     }
    //     if(choice == 10)
    //         break;
    // }
    // getch();
    // endwin();

    return 0;
}
