#include <ncurses.h>
#include <string>
#include "backpack.hpp"
#include "brand.hpp"

using namespace std;

class menu_item
{
private:

public:
    menu_item(string title, char trigger);
    ~menu_item();
    
    string text;
    char trigger;
};

class menu
{
private:
    WINDOW *win;
    menu_item* items;
    int num_items;
public:
    int highlight = 0;

    menu(WINDOW *win, menu_item *items, int num_items);
    ~menu();
    void draw();
    void handle_trigger(WINDOW *listwin);
};

class list_item{
    private:
        Backpack *item;
    public:
        int height: 3;
        list_item(Backpack *item){
            this->item = item;
        };
        ~list_item();
        void draw(WINDOW *listwin, int cur_pos) {
            Brand brand = item->GetBrand();
            int x_pos = 1;
            mvwprintw(listwin, cur_pos+1, x_pos, "Brand:");
            x_pos += 7;
            mvwprintw(listwin, cur_pos+1, x_pos, brand.name_brand.c_str());
            x_pos += brand.name_brand.length() + 3;
            mvwprintw(listwin, cur_pos+1, x_pos, "Country:");
            x_pos += 8;
            mvwprintw(listwin, cur_pos+1, x_pos, brand.country.c_str());

        };
};

class search_list{
    private:

    public:
        int first_item;
        int cur_pos;
        WINDOW *listwin;
        Backpack *items;
        int num_items;

        search_list(WINDOW *listwin, Backpack *items, int num_items) {
            this->listwin = listwin;
            this->items = items;
            this->num_items = num_items;
            cur_pos = 0;
            first_item = 0;
        };

        ~search_list(){
            
        };

        void draw(){
            int y_max, x_max;
            getmaxyx(listwin, y_max, x_max);
            
            wclear(listwin);
            for (int i = 0; i < num_items; i++) {
                if(cur_pos < y_max) {
                    items[i].draw(listwin, cur_pos);
                    cur_pos += 3;
                } else {
                    break;
                }
            }
            wrefresh(listwin);
            
        };
};